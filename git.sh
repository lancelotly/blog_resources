#!/bin/bash
echo "Please wait, we will finish update soon!"
date
git pull
echo "Processing..."
git add -A
echo "Processing..."
git commit -m "update"
echo "Processing..."
git push --progress -u origin master
date
echo "Done! 🎶"
